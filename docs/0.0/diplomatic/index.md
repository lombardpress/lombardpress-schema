---
layout: page
title:  "Lombard Press Schema 0.0.0: Diplomatic File Specifications"
date:   2016-04-01
categories: schema
---

Version 0.0.0 is being depreciated. We provide a list of basic decisions made here to offer and transparency and clarity about past decisions, so as to aid in graduation of all files to version 1.0.0