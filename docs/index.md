---
layout: page
title:  Schema Home
date:   2016-04-01 09:46:33 -0400
categories: schema
---

# The Lombard Press Schema

## Version 1.0.0 (in preparation)

* [1.0.0-critical](1.0/critical/)
* [1.0.0-diplomatic](1.0/diplomatic/)

## Version 0.0.0 (depreciated)

* [0.0.0-critical](0.0/critical/)
* [0.0.0-diplomatic](0.0/diplomatic/)

To submit a bug report, ask a question about how to encode a particular text, or make a feature request, please submit an issue here: [https://github.com/lombardpress/lombardpress-schema/issues](https://github.com/lombardpress/lombardpress-schema/issues)